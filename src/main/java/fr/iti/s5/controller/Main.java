package fr.iti.s5.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.iti.s5.dao.User;
import fr.iti.s5.model.Modele;

@WebServlet( name="Main", urlPatterns = "/Main" )
public class Main extends HttpServlet  {

	/*
	  A expliquer
	 */
	private static final long serialVersionUID = 1L;
	
	@Override
	public void init() throws ServletException {
	}

	@Override
	protected void doGet(HttpServletRequest pRequete, HttpServletResponse pResponse) throws ServletException, IOException {
	//	this.getServletContext().getRequestDispatcher( "/WEB-INF/auth.jsp" ).forward( pRequete, pResponse );
//Récupération du parametre permettant de savoir depuis quelle page le controleur a été appelé
		String from = pRequete.getParameter("from");
		String to = null;
		try {
			Modele modele = new Modele();
			User user = modele.getUser(pRequete.getParameter("name"));
			if(null != from && from.equals("0") && user != null) {
//Cas 1 : success. from => login.jsp , to => auth.jsp  
//Cas 2 : failed. from => login.jsp , to => login.jsp
//Appel au Modele et verification des informations de connexion login et mot de passe.
				if(modele.connect(user, pRequete.getParameter("password"))) {
					to = "auth.jsp";	
					
//Problème : Si on revient sur la page login doit on se reconnecter à chaque fois ?
// Trouver une solution afin de gérer les informations de connexion. 
// Voir dans ce cas l'objet : HttpSession
				}else {
					to = "login.jsp";
				}
			}else if(null != from && from.equals("1")) {
				//Cas 1 : success. from => auth.jsp , to => auth.jsp  
//Cas 2 : failed. from => update.jsp , to => login.jsp
				
//Problèmatique : avant d'arriver dans cette page, 
// il faut être sur qu'on est passé par la page de login et qu'on est bien connecté
				to = "update.jsp";
			}else {
				to = "error.jsp";
			}
			
//Redirection vers la page de résultat après traitement de la requete
			System.out.println("Oui je passe bien par ici");
			//pRequete.getRequestDispatcher("WEB-INF/error.jsp").forward(pRequete, pResponse);
			pRequete.getRequestDispatcher(to).forward(pRequete, pResponse);
		} catch (Exception e) {
			System.out.println("Je passe par l'exeption");
			e.printStackTrace();
		}
			
	}
	
	@Override
	protected void doPost(HttpServletRequest pRequete, HttpServletResponse pResponse) throws ServletException, IOException {
		doGet(pRequete, pResponse);
	}
	
	
}

