package fr.iti.s5.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MySQLConnection {

    private static Connection connection;
    String url = "jdbc:mysql://localhost:3306/jwebmanageuser";
    //String user = "lesmanchots";
    //String password = "a2mains";
    String user = "root";
    String password = "";

    private MySQLConnection() {
        String methodName = "Constructeur MySQLConnection";

        try {
        	Class.forName("com.mysql.jdbc.Driver").newInstance();
            connection = DriverManager.getConnection(url, user, password);
        } catch (ClassNotFoundException e) {
            System.out.println("ClassNotFoundException : " + methodName + ", Exception : " + e.getMessage());
        }
        catch(SQLException e ){
            System.out.println("Exception : " + e.getMessage());
        }
        catch(IllegalAccessException e)
        {
        	System.out.println(" Exception : " + e.getMessage());
        }
        catch(InstantiationException e)
        {
        	System.out.println(" Exception : " + e.getMessage());
        }
    }

    public static Connection getInstance() {
        String methodName = "getInstance";
        if (connection == null) {
            MySQLConnection mySQLConnection = new MySQLConnection();
            System.out.println("Instanciation de MySqlConnection");
        } else {
            System.out.println("Re-Utilisation d'une instance MySqlConnection.");
        }
        return connection;
    }
}
