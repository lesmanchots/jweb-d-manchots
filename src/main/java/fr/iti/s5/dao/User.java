package fr.iti.s5.dao;

import java.time.LocalDate;

public class User {
	
	private int id;
	private String username;
	private String password;
	private String name;
	private String firstname;
	private LocalDate birthdate;
	private String email;
	
	

	/**
	 * @param id
	 * @param username
	 * @param password
	 * @param name
	 * @param firstname
	 * @param birthdate
	 * @param email
	 */
	public User(int id, String name, String firstname, LocalDate birthdate, String password, String username, String email) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.name = name;
		this.firstname = firstname;
		this.birthdate = birthdate;
		this.email = email;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the firstname
	 */
	public String getFirstname() {
		return firstname;
	}

	/**
	 * @param firstname the firstname to set
	 */
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	/**
	 * @return the birthdate
	 */
	public LocalDate getBirthdate() {
		return birthdate;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	
	@Override
    public String toString() {
        return "//============ \nUtilisateur :\n" + "ID : " + id + ", NOM : " + name + " PRÉNOM : " + firstname + "\nDATE DE NAISSANCE  : " + birthdate + ", MOT DE PASSE : " + password + ", IDENTIFIANT : " + username + ", EMAIL  : " + email + "\n ============//";
    }

	/**
	 * @param birthdate the birthdate to set
	 */
	public void setBirthdate(LocalDate birthdate) {
		this.birthdate = birthdate;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

}
