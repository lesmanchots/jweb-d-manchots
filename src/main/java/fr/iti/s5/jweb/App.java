package fr.iti.s5.jweb;

import fr.iti.s5.dao.MySQLConnection;
import fr.iti.s5.dao.User;
import fr.iti.s5.model.Modele;

public class App 
{
    public static void main( String[] args )
    {

		MySQLConnection.getInstance();
    	Modele m = new Modele();
    	User user = m.getUser("lolo");
    	String password = "laul";
    	//Boolean canConnect = m.connect(user, password);
    	Boolean canConnect = m.connect(user, user.getPassword());
    	String message = null;
    	
    	if(canConnect == true) {
    		message = "L'utilisateur peut se connecter.\nBonjour " + user.getFirstname() + " " + user.getName();
    	}
    	else {
    		message = "Un mauvais mot de passe a été entré. \nL'utilisateur ne peut pas se connecter";
    	}
    	System.out.println(message);
    }
}
