package fr.iti.s5.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import fr.iti.s5.dao.MySQLConnection;
import fr.iti.s5.dao.User;

public class Modele {

	Connection connection = null;
//	
//	public Modele() throws ClassNotFoundException, SQLException{
//		Class.forName("com.mysql.jdbc.Driver");
//		connection = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/jwebmanageuser","root","");
//		setConnection(connection);
//	}
//	
//	public Modele(Connection pConnection){
//		setConnection(pConnection);
//	}
//	
//	public Connection getConnection(){
//		return this.connection;
//	}
//	public void setConnection(Connection connection){
//		this.connection = connection;
//	}	
	
	public Boolean connect(User user, String enteredPassword){
		Boolean canConnect = false;
		
		User testedUser = this.getUser(user.getUsername());
		if(testedUser != null) {
			if(testedUser.getPassword().equals(enteredPassword)) {
				canConnect = true;
			}
		}
		
		return canConnect;		
	}
	
	public User getUser(String username){
		String methodName = "displayChosenUser";
        User user = null;
        try {
            String sqlCommand = "select * from user where user.username=?";
            ResultSet result = null;
            PreparedStatement preparedStatement = null;
            try {
            	preparedStatement = MySQLConnection.getInstance().prepareStatement(sqlCommand);
                preparedStatement.setString(1, username);
                result = preparedStatement.executeQuery();
                while (result.next()) {
                	
                	Date birthdate = result.getDate("birthdate");
                	Instant instant = Instant.ofEpochMilli(birthdate.getTime());
                	LocalDate correctBirthdate = LocalDateTime.ofInstant(instant, ZoneId.systemDefault()).toLocalDate();
                	user = new User(result.getInt("id"), result.getString("name"), result.getString("firstname"), correctBirthdate, result.getString("password"), result.getString("username"), result.getString("email"));
                    System.out.println(user);
                }

            } finally {
                if (result != null) {
                    result.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
            }
        } catch (SQLException e) {
            System.out.println("Erreur lors de l'excecution de la methode " + methodName + " , Exception: " + e.getMessage());
        }
        return user;
	}
	
	public void setUser(){
		
	}
	

}
